
Preliminary Programming Project
==============================

Requirements
------------
* java, javac, javadoc, jar (code compatible with Java Platform SE 8*)
* ant (script compatible with Apache Ant 1.9.3)
* junit (code compatible with JUnit 4.6.2)
* generic text file reader (ASCII or UTF-8)
* generic PDF reader (compatible with PDF 1.6)
* generic HTML renderer (compatible with HTML 4)
* generic JPEG, GIF, PNG renderers

*Although Java 8 has several desirable new features, Java 7 may be a safer
 choice to enhance the likelihood of compatibility with test systems.

Git
---
Copies the repo to your machine
```sh
git clone <https url from bitbucket>
```

Switch to the develop branch where they latest code is
```sh
git checkout develop
```

Change to a new branch with the name feature/<something your working on>
```sh
git checkout -b feature/<something your working on>
```

When changes are complete, or you would like to save them in the repo

Stage all changes for commit
```sh
git add -A
```

Create a commit
```sh
git commit -m "<your message>"
```

You don't have to do this, but it helps, copy the name of the branch you are on.
```sh
git branch
```

Push to the repo
```sh
git push origin feature/<something your working on>
```

Licence
-------
