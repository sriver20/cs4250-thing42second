package com.code.sd1.main;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author David Gerells
 *         Test class for Thing42 object.
 *         Testing is not rigorous but checks to make sure functionality
 *         is working.
 *         Bugs always exist. More testing is always a plus.
 *         Other members please add any other tests that you think I missed.
 */
public class Thing42Test
{

    /**
     * Things 1 through 4 used for tests.
     */
    private Thing42<Integer, String> thing1, thing2, thing3, thing4;

    /**
     * Used to test things with different generics.
     */
    private Thing42<String, Integer> thing5;


    /**
     * @throws Exception if there was an issue with setup
     */
    @Before
    public final void setUp() throws Exception {
        thing1 = new Thing42<Integer, String>(24, 100l, "Green");
        // and
        thing2 = new Thing42<Integer, String>(23, 100l, "Eggs");

        thing3 = new Thing42<Integer, String>(22, 32l, "And");
        // and
        thing4 = new Thing42<Integer, String>(22, -51l, "Ham");

        thing5 = new Thing42<String, Integer>("Once", 12, new Integer(12));
    }

    /**
     * @throws java.lang.Exception if there was an issue with takeown
     */
    @After
    public void tearDown() throws Exception
    {

    }

    /**
     * Test method for {@link Thing42#addPeer(Thing42orNull)}.
     */
    @Test
    public final void testAddPeer()
    {
        thing1.addPeer(thing2);
        assertTrue(thing1.getOnePeer(thing2.getKey()).equals(thing2));
    }

    /**
     * Test method for {@link Thing42#addPeer(Thing42orNull)}.
     */
    @Test(expected = NullPointerException.class)
    public final void testAddPeerNull()
    {
        thing1.addPeer(null);
    }

    /**
     * Test method for {@link Thing42#appendToPool(Thing42orNull)}.
     */
    @Test
    public final void testAppendToPool()
    {
        thing1.appendToPool(thing2);
        assertTrue(thing1.removeFromPool(thing2));
    }

    /**
     * Test method for {@link Thing42#appendToPool(Thing42orNull)}.
     */
    @Test(expected = NullPointerException.class)
    public final void testAppendToPoolNull()
    {
        thing1.appendToPool(null);
    }

    /**
     * Test method for {@link Thing42#getData()}.
     */
    @Test
    public final void testGetData()
    {
        assertTrue(thing1.getData().equals("Green"));
        assertTrue(thing2.getData().equals("Eggs"));
        assertTrue(thing3.getData().equals("And"));
        assertTrue(thing4.getData().equals("Ham"));
        assertTrue(thing5.getData().equals(12));
    }

    /**
     * Test method for {@link Thing42#getKey()}.
     */
    @Test
    public final void testGetKey()
    {
        assertTrue(thing1.getKey().equals(24));
        assertTrue(thing2.getKey().equals(23));
        assertTrue(thing3.getKey().equals(22));
        assertTrue(thing4.getKey().equals(22));
        assertTrue(thing5.getKey().equals("Once"));
        Thing42<TestImmutable, Integer> test1 = new Thing42<TestImmutable, Integer>(
                new TestImmutable(5), 0, 5);
        TestImmutable test2 = test1.getKey();
        assertTrue(test1.getKey().var == 5);
        test2.var = 6;
        assertTrue(test1.getKey().var == 5);
    }

    /**
     * Test method for {@link Thing42#getLevel()}.
     */
    @Test
    public final void testGetLevel()
    {
        assertTrue(thing1.getLevel() == 100);
        assertTrue(thing2.getLevel() == 100);
        assertTrue(thing3.getLevel() == 32);
        assertTrue(thing4.getLevel() == -51);
        assertTrue(thing5.getLevel() == 12);
    }

    /**
     * Test method for {@link Thing42#getOnePeer(java.lang.Object)}.
     */
    @Test
    public final void testGetOnePeer()
    {
        thing1.addPeer(thing2);
        assertTrue(thing1.getOnePeer(thing2.getKey()).equals(thing2));
    }

    /**
     * Test method for {@link Thing42#getPeersAsCollection()}.
     */
    @Test
    public final void testGetPeersAsCollectionKey()
    {
        Thing42<Integer, String> thing6 = new Thing42<Integer, String>(12, 0,
                "Green eggs and ham");
        thing6.addPeer(thing2);
        thing6.addPeer(thing3);
        thing6.addPeer(thing4);
        thing6.addPeer(thing1);
        assertTrue(thing6.getPeersAsCollection(22).contains(thing3));
        assertTrue(thing6.getPeersAsCollection(22).contains(thing4));
        assertFalse(thing6.getPeersAsCollection(22).contains(thing5));
    }

    /**
     * Test method for {@link Thing42#getPeersAsCollection(java.lang.Object)}.
     */
    @Test
    public final void testGetPeersAsCollectionNoKey()
    {
        Thing42<Integer, String> thing6 = new Thing42<Integer, String>(12, 0,
                "Green eggs and ham");
        thing6.addPeer(thing2);
        thing6.addPeer(thing3);
        thing6.addPeer(thing4);
        assertTrue(thing6.getPeersAsCollection().contains(thing3));
        assertTrue(thing6.getPeersAsCollection().contains(thing4));
        assertFalse(thing6.getPeersAsCollection().contains(thing1));
    }

    /**
     * Test method for {@link Thing42#getPoolAsList()}.
     */
    @Test
    public final void testGetPoolAsList()
    {
        Thing42<Integer, String> thing6 = new Thing42<Integer, String>(12, 0,
                "Green eggs and ham");
        assertTrue(thing6.getPoolAsList().size() == 0);
        thing6.appendToPool(thing2);
        thing6.appendToPool(thing3);
        thing6.appendToPool(thing4);
        assertTrue(thing6.getPoolAsList().size() == 3);
        thing6.appendToPool(thing1);
        assertTrue(thing6.getPoolAsList().size() == 4);
        List<Thing42orNull<Integer, String>> list = thing6.getPoolAsList();
        assertTrue(list.contains(thing2));
        assertTrue(list.contains(thing3));
        assertTrue(list.contains(thing4));
    }

    /**
     * Test method for {@link Thing42#removeFromPool(Thing42orNull)}.
     */
    @Test
    public final void testRemoveFromPool()
    {
        // Test single instance in pool
        Thing42<Integer, String> thing6 = new Thing42<Integer, String>(12, 0,
                "Green eggs and ham");
        thing6.appendToPool(thing2);
        thing6.appendToPool(thing3);
        thing6.appendToPool(thing4);
        assertTrue(thing6.removeFromPool(thing2));
        // test multi instance
        thing6.appendToPool(thing2);
        thing6.appendToPool(thing2);
        thing6.appendToPool(thing2);
        assertTrue(thing6.removeFromPool(thing2));
        assertTrue(thing6.getPoolAsList().contains(thing2));
    }

    /**
     * Test method for {@link Thing42#removeFromPool(Thing42orNull)}.
     */
    @Test(expected = NullPointerException.class)
    public final void testRemoveFromPoolNull()
    {
        thing1.removeFromPool(null);
    }

    /**
     * Test method for {@link Thing42#removePeer(Thing42orNull)}.
     */
    @Test
    public final void testRemovePeer()
    {
        Thing42<Integer, String> thing6 = new Thing42<Integer, String>(12, 0,
                "Green eggs and ham");
        thing6.addPeer(thing2);
        thing6.addPeer(thing3);
        thing6.addPeer(thing4);
        // test single instance in peers
        assertTrue(thing6.getOnePeer(thing2.getKey()) != null);
        assertTrue(thing6.removePeer(thing2));
        assertTrue(thing6.getOnePeer(thing2.getKey()) == null);
        assertTrue(thing6.getOnePeer(thing3.getKey()) != null);
        assertTrue(thing6.getOnePeer(thing4.getKey()) != null);
        // test multi instance in peers however a set cannot have duplicates
        // so...
        thing6.addPeer(thing2);
        thing6.addPeer(thing2);
        thing6.addPeer(thing2);
        thing6.addPeer(thing2);
        thing6.addPeer(thing2);
        thing6.addPeer(thing2);
        thing6.addPeer(thing2);
        assertTrue(thing6.getOnePeer(thing2.getKey()) != null);
        assertTrue(thing6.removePeer(thing2));
        assertFalse(thing6.getPeersAsCollection().contains(thing2));
    }

    /**
     * Test method for {@link Thing42#removePeer(Thing42orNull)}.
     */
    @Test(expected = NullPointerException.class)
    public final void testRemovePeerNull()
    {
        thing1.removePeer(null);
    }

    /**
     * Test method for {@link Thing42#setData(java.lang.Object)}.
     */
    @Test
    public final void testSetData()
    {

        assertTrue(thing1.getData().equals("Green"));
        thing1.setData("Hat");
        assertTrue(thing1.getData().equals("Hat"));
    }

    /**
     * Test method for equals.
     */
    @Test
    public final void testEquals()
    {
        Thing42orNull<Integer, String> t1 = new Thing42<Integer, String>(12,
                25L, "Cat");
        Thing42orNull<Integer, String> t2 = new Thing42<Integer, String>(12,
                25L, "Cat");
        assertTrue("Empty Thing42 failed equality", t1.equals(t2));
        assertTrue("Empty Thing42 was equal to null", !t1.equals(null));

        // Begin basic tests with each other in lists
        // and self in lists
        t1.addPeer(t1);
        t2.addPeer(t1);
        System.out.println(""
                + (t1.getPeersAsCollection().hashCode() == t2
                        .getPeersAsCollection().hashCode()));
        assertTrue(t1.equals(t2));
        assertTrue(t2.equals(t1));

        t1 = new Thing42<Integer, String>(12, 25L, "Cat");
        t2 = new Thing42<Integer, String>(12, 25L, "Cat");
        t1.addPeer(t2);
        t2.addPeer(t2);
        assertTrue(t1.equals(t2));
        assertTrue(t2.equals(t1));

        t1 = new Thing42<Integer, String>(null, 25L, "");
        t2 = new Thing42<Integer, String>(null, 25L, "");
        thing1.addPeer(thing2);
        thing3.addPeer(thing4);
        t1.appendToPool(thing1);
        t1.appendToPool(thing3);
        t2.appendToPool(thing1);
        t2.appendToPool(thing3);
        assertTrue(t1.equals(t2));
        assertTrue(t2.equals(t1));

        t1 = new Thing42<Integer, String>(12, 25L, "Cat");
        t2 = new Thing42<Integer, String>(12, 25L, "Cat");
        t1.addPeer(t1);
        t2.addPeer(t1);
        assertTrue(t1.equals(t2));
        assertTrue(t2.equals(t1));

        t1 = new Thing42<Integer, String>(12, 25L, "Cat");
        t2 = new Thing42<Integer, String>(12, 25L, "Cat");
        t1.addPeer(t2);
        t2.addPeer(t1);
        assertTrue(t1.equals(t2));
        assertTrue(t2.equals(t1));

        // test with many objects in list and pool
        t1 = new Thing42<Integer, String>(12, 25L, "Cat");
        t2 = new Thing42<Integer, String>(12, 25L, "Cat");
        t1.addPeer(thing1);
        t1.addPeer(thing2);
        t1.addPeer(thing3);
        t1.addPeer(thing1);
        t1.addPeer(t2);
        t1.appendToPool(thing1);
        t1.appendToPool(t1);

        t2.addPeer(thing1);
        t2.addPeer(thing2);
        t2.addPeer(thing3);
        t2.addPeer(thing1);
        t2.addPeer(t2);
        t2.appendToPool(thing1);
        t2.appendToPool(t1);
        assertTrue(t1.equals(t2));
        assertTrue(t2.equals(t1));

        t1 = new Thing42<Integer, String>(12, 25L, "Cat");
        t2 = new Thing42<Integer, String>(12, 25L, "Cat");
        t1.addPeer(thing2);
        t1.addPeer(thing3);
        t1.addPeer(thing1);
        t1.addPeer(t2);
        t1.appendToPool(thing1);
        t1.appendToPool(t1);

        t2.appendToPool(t1);
        assertTrue(!t1.equals(t2));
        assertTrue(!t2.equals(t1));

        t1 = new Thing42<Integer, String>(12, 20L, "Cat");
        t2 = new Thing42<Integer, String>(12, 25L, "Cat");
        assertTrue(!t1.equals(t2));
        assertTrue(!t2.equals(t1));

        t1 = new Thing42<Integer, String>(2, 25L, "Cat");
        t2 = new Thing42<Integer, String>(12, 25L, "Cat");
        assertTrue(!t1.equals(t2));
        assertTrue(!t2.equals(t1));

        t1 = new Thing42<Integer, String>(12, 25L, "2");
        t2 = new Thing42<Integer, String>(12, 25L, "Cat");
        assertTrue(!t1.equals(t2));
        assertTrue(!t2.equals(t1));

        t1 = new Thing42<Integer, String>(1, 20L, "1");
        t2 = new Thing42<Integer, String>(12, 25L, "Cat");
        assertTrue(!t1.equals(t2));
        assertTrue(!t2.equals(t1));

    }

    /**
     * Test method for hashcode
     */
    @Test
    public final void testHashCode()
    {
        Thing42orNull<Integer, String> t1 = new Thing42<Integer, String>(12,
                25L, "Cat");
        Thing42orNull<Integer, String> t2 = new Thing42<Integer, String>(12,
                25L, "Cat");
        assertTrue(
                "Empty Thing42 objects didn't have same hash when they should have",
                t1.hashCode() == t2.hashCode());

        t1.addPeer(t1);
        t2.addPeer(t1);
        assertTrue(t1.hashCode() == t2.hashCode());

        t1 = new Thing42<Integer, String>(12, 25L, "Cat");
        t2 = new Thing42<Integer, String>(12, 25L, "Cat");
        t1.addPeer(t2);
        t2.addPeer(t2);
        assertTrue(t1.hashCode() == t2.hashCode());

        t1 = new Thing42<Integer, String>(12, 1L, "Cat");
        t2 = new Thing42<Integer, String>(12, 1L, "Cat");
        t1.addPeer(t2);
        t2.addPeer(t1);
        assertTrue(t1.hashCode() == t2.hashCode());

        t1 = new Thing42<Integer, String>(12, 25L, "2");
        t2 = new Thing42<Integer, String>(12, 25L, "2");
        t1.addPeer(thing1);
        t2.addPeer(thing1);
        assertTrue(t1.hashCode() == t2.hashCode());

        t1 = new Thing42<Integer, String>(12, 25L, "Cat");
        t2 = new Thing42<Integer, String>(12, 25L, "Cat");
        t1.addPeer(thing1);
        t1.addPeer(thing3);
        assertTrue(t1.hashCode() != t2.hashCode());

        // different size() of peers
        t1 = new Thing42<Integer, String>(12, 25L, "Cat");
        t2 = new Thing42<Integer, String>(12, 25L, "Cat");
        t1.addPeer(thing1);
        t2.addPeer(thing1);
        t2.addPeer(thing3);
        assertFalse(t1.hashCode() == t2.hashCode());

        // 2 objects have the different sizes of pool but have the same
        // hashCode????
        t1 = new Thing42<Integer, String>(12, 25L, "Cat");
        t2 = new Thing42<Integer, String>(12, 25L, "Cat");
        t1.appendToPool(thing1);
        t2.appendToPool(thing1);
        t2.appendToPool(thing3);
        assertFalse(t1.hashCode() == t2.hashCode());
    }

    /**
     * Class used to test immutability of keys
     * More so makes sure that the clone method is called on an object that
     * implements it.

     * @author David
     */
    private class TestImmutable implements Cloneable {
        /**
         * Simple var to check mutable.
         */
        public int var;

        /**
         * Simple Constructor.
         * @param t the value for var
         */
        public TestImmutable(final int t) {
            var = t;
        }

        @Override
        public Object clone() throws CloneNotSupportedException        {
            return new TestImmutable(var);
        }
        
    }

}
