package com.code.sd1.main;
/**
 * @author David Gerrells
 *         Simple example of Thing42 objects inn action.
 *         Nothing fancy.
 */
public class Thing42Example
{
    public static void main(String[] args)
    {
        Thing42<String, String> thing1 = new Thing42<String, String>("Golden",
                12L, "Thing1");
        Thing42<String, String> thing2 = new Thing42<String, String>("Silver",
                32L, "Thing2");
        Thing42<String, String> cat = new Thing42<String, String>("In", 12L,
                "Hat");
        cat.appendToPool(thing1);
        cat.appendToPool(thing2);
        thing1.addPeer(thing2);
        thing2.addPeer(thing1);

        print("The cat has " + cat.getPoolAsList().size() + " things.");
        print("They are " + cat.getPoolAsList().get(0).getData() + " and "
                + cat.getPoolAsList().get(1).getData() + ".");
        print(thing1.getData() + " has a " + thing1.getKey() + " key.");
        print(thing2.getData() + " has a " + thing2.getKey() + " key.");
        print("The cat is in a " + cat.getData());
    }

    /**
     * Smaller print method...
     * 
     * @param s
     */
    private static void print(String s)
    {
        System.out.println(s);
    }
}
