package com.code.sd1.main;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author David Gerrells An Object that implements the Thing42orNull interface.
 * @param <K>
 *            key type
 * @param <D>
 *            data type
 */
public class Thing42<K, D> implements Thing42orNull<K, D> {

    /**
     * The Key of this Thing42.
     */
    private K key;
    /**
     * The level of this thing42.
     */
    private long level;

    /**
     * The Data of this Thing42.
     */
    private D data;
    /**
     * It was never specified if the peers are mutable or not so we assume they
     * are mutable. That means that the actual set is given in the
     * getPeersAsCollection method.
     */
    private Set<Thing42orNull<K, D>> peers;
    /**
     * It was never specified if the pool is mutable or not so we assume it is
     * mutable. That means that the actual list is given in the
     * getPoolAsCollection method.
     */
    private List<Thing42orNull<K, D>> pool;

    /**
     * Constructor for a Thing42.
     * @param key1
     *            the key for this Thing42 object
     * @param levelIn
     *            the level of this Thing42 object
     * @param dataIn
     *            the data of this Thing42 object
     */
    public Thing42(final K key1, final long levelIn, final D dataIn) {
        this.key = key1;
        this.level = levelIn;
        this.data = dataIn;
        peers = new HashSet<Thing42orNull<K, D>>();
        pool = new ArrayList<Thing42orNull<K, D>>();
    }

    @Override
    public final void addPeer(final Thing42orNull<K, D> newPeer) {
        if (newPeer == null) {
            throw new NullPointerException();
        } else {
            peers.add(newPeer);
        }
    }

    @Override
    public final void appendToPool(final Thing42orNull<K, D> newMember) {
        if (newMember == null) {
            throw new NullPointerException();
        } else {
            pool.add(newMember);
        }
    }

    @Override
    public final D getData() {
        return data;
    }

    @Override
    public final K getKey() {
        // Call a method to check if the generic has a clone method
        // If so, return that clone.
        // It is not our responsibility for a deep copy.
        return clone(key);
    }

    @Override
    public final long getLevel() {
        return new Long(level);
    }

    @Override
    public final Thing42orNull<K, D> getOnePeer(final K key1) {
        for (Thing42orNull<K, D> t : peers) {
            if (t.getKey().equals(key1)) {
                return t;
            }
        }
        return null;
    }

    @Override
    public final Collection<Thing42orNull<K, D>> getPeersAsCollection() {
        return peers;
    }

    @Override
    public final Collection<Thing42orNull<K, D>>
    getPeersAsCollection(final K key1) {
        ArrayList<Thing42orNull<K, D>> listReturn
        = new ArrayList<Thing42orNull<K, D>>();
        for (Thing42orNull<K, D> t : peers) {
            if (t.getKey().equals(key1)) {
                listReturn.add(t);
            }
        }
        return listReturn;
    }

    @Override
    public final List<Thing42orNull<K, D>> getPoolAsList() {
        return pool;
    }

    @Override
    public final boolean removeFromPool(final Thing42orNull<K, D> member) {
            if (member == null) {
                throw new NullPointerException();
            }
            // not sure if we use the Key from the member to check against other
            // members
            // instead will default to the equals method.
            return pool.remove(member);
    }

    @Override
    public final boolean removePeer(final Thing42orNull<K, D> peer) {
        if (peer == null) {
            throw new NullPointerException();
        }
        // not sure if we use the Key from the member to checks against other
        // members
        // instead will default to the equals method.
        return peers.remove(peer);
    }

    @Override
    public final void setData(final D newData) {
        this.data = newData;
    }

    @Override
    public final int hashCode() {
        final int prime = 3;
        // these could be null
        String dataHash, keyHash;
        if (data == null) {
            dataHash = "datanull";
        } else {
            dataHash = data.hashCode() + "";
        }
        if (key == null) {
            keyHash = "keyNull";
        } else {
            keyHash = key.hashCode() + "";
        }
        return ("" + keyHash + dataHash + level
                + ((peers.size() + pool.size() + prime) % prime))
                .hashCode();
    }

    @Override
    public final boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        // checking if obj is the right type of class be for casting
        if (!(obj instanceof Thing42orNull)) {
            return false;
        }
        @SuppressWarnings("unchecked")
        Thing42orNull<K, D> other = (Thing42orNull<K, D>) obj;
        // early exit
        if (other == this) {
            return true;
        }
        if (other.getPeersAsCollection().size() != peers.size()) {
            return false;
        }
        if (other.getPoolAsList().size() != pool.size()) {
            return false;
        }
        // null checks
        if (key == null && other.getKey() != null) {
            return false;
        }
        if (data == null && other.getData() != null) {
            return false;
        }
        if (key != null && other.getKey() == null) {
            return false;
        }
        if (data != null && other.getData() == null) {
            return false;
        }
        // main checks
        if (level != other.getLevel()) {
            return false;
        }
        if (other.getData() != null && !other.getData().equals(data)) {
            return false;
        }
        if (other.getKey() != null && !other.getKey().equals(key)) {
            return false;
        }
        if (!getPoolAsList().equals(other.getPoolAsList())) {
            return false;
        }
        if (getPeersAsCollection().hashCode() != other.getPeersAsCollection()
                .hashCode()) {
            return false;
        }
        // other is equal
        return true;
    }

    /**
     * Will try and call the clone method of key if it exists. Returns the
     * argument if none no clone method.
     * @param k the key to try and clone
     * @return the cloned object
     */
    @SuppressWarnings("unchecked")
    private K clone(final K k) {
        if (k == null) {
            return k;
        }
        try {
            for (Class<?> i : k.getClass().getInterfaces()) {
                if (i.getName().equals("java.lang.Cloneable")) {
                    for (Method m : k.getClass().getMethods()) {
                        if (m.getName().equals("clone")
                                && m.getParameterTypes().length == 0) {
                            return (K) m.invoke(k);
                        }
                    }
                }
            }
        } catch (IllegalAccessException | IllegalArgumentException
                | InvocationTargetException e) {
            e.printStackTrace();
        }
        return k;
    }

}
